#from SQLModleCore import db.sesion
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from SQLModleCore import Agent, Commands
import string, random
import os
import json
from SQLModleCore import db

app = Flask(__name__)
"""
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/natengaki/Documents/I3/Red_Teams/register/test.db'
db = SQLAlchemy(app)
#db = SQLAlchemy(app)
db.init_app(app)
app.app_context().push()

if "test.db" not in os.listdir("./register/"):
    db.create_all()
else:
    pass

"""
if "test.db" not in os.listdir("./register/"):
    db.create_all()
else:
    pass

def createName(db,length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    #result_str = "baaalo"

    #name = db.session.query(Agent.noms).all()
    #agent = db.session.filter(Agent)
    name = db.session.query(Agent).filter_by(noms=result_str).all()
    if len(name) == 0:
    #if len("10") == 0:
        #print(result_str)
        return result_str
    else :
        print("Non")
        createName(length)

#def createAgent(ordre_=[],noms_=createName(10),os_=None):
def createAgent(db,ordre_=[], noms_="toto", os_=None):
    noms_ = createName(db,10)
    var = Agent(ordre=ordre_,noms=noms_,os=os_)
    db.session.add(var)
    db.session.commit()

    print('id :',var.id)
    print(var.os)
    print("le nom :", var.noms)
    print(var.ordre)

    idi = var.id
    return idi

def createAgentVide(db,ordre_=[],noms_=None,os_=None):
    var = Agent(ordre=ordre_,noms=noms_,os=os_)
    db.session.add(var)
    db.session.commit()

def createorder(db,agentID=1, orde_=None, data_="wait", status_=False, resultat_=None): #erreur dans la db ma flèmme
    #ag = db.session.query(Agent).filter_by()
    var = Commands(person_id=agentID,data=data_,status=status_,orde=orde_,resultat=resultat_)
    print("le type",type(var))
    db.session.add(var)
    db.session.commit()

    print('id_ordre :',var.id_commands)
    print("la commande :", var.data)
    print("l'agent qui a l'ordre : ",var.person_id)
    id_ordre = var.id_commands
    bashh=var.data
    id_agent=var.person_id
    data = {"Agent": var.person_id, "id_commands": var.id_commands,
            "commande_exe": var.data}
    return id_ordre,bashh,id_agent, json.dumps(data)

def testdb(db):
    return db.session.query(Agent.noms).all()

def listerAgentJson(db):
    agentpack = db.session.query(Agent).order_by(Agent.id).all()
    for i in range(len(agentpack)):
        name = agentpack[i].noms
        di=agentpack[i].id
        #print(name +" ", di)
        dictjson = {
            "Agent_Name": agentpack[i].noms,
            "ID_agent": agentpack[i].id
        }
        jsonString = json.dumps(dictjson)
        with open(f'./data/agent_list.json', 'a') as clientfile:
            clientfile.write(jsonString)
            clientfile.close()
        with open(f'./data/agent_list.json', 'r') as clientfile:
            content = clientfile.read()
            clientfile.close()
        js = json.loads(content)
        try:
            return js
        except:
            print(f"Il y a problème")

def listerAgent(db):
    #donne
    agentpack = db.session.query(Agent).order_by(Agent.id).all()
    data = {}
    for a in agentpack:
        # Ajoute l'agent au dictionnaire avec son nom et son ID
        data[a.noms] = {'Agent_name': a.noms, 'ID_agent': a.id}
    # Convertit le dictionnaire en format JSON
    return json.dumps(data)


# donne le premier dans la liste d'exécution par l'agent qui demande l'ordre
def get_Ordre(db,id_agent):
    agent = db.session.query(Agent).filter_by(id=id_agent).first()
    data={}
    if not agent:
        return 'Agent not found'
    command = db.session.query(Commands).filter_by(person_id=agent.id, status=False).first()
    if command is not None:
        data[1]= {"Agent": command.person_id, "id_commands": command.id_commands, "commande_status": command.status,
                              "commande_exe": command.data}
        return command.id_commands, command.data, command.person_id, json.dumps(data)
    if not command :
        return  "No commands"


# Donne les info de tout les ordre que doit exécuter un agent etat(all / todo / done)
def get_all_order_agent(db,id_agent,etat="todo"):
    f = db.session.query(Agent).filter_by(id=id_agent).first()
    print(f)
    list_ordre_todo = dict()
    data ={}
    if f is not None:
        o = f.ordre
        print(o)
        if o != None:
            #list_ordre_todo = dict() ancien empalcement
            for i in range(len(o)):
                if etat =="todo":
                    if o[i].status == False and o[i].data != None:
                        #print(o[i])
                        print("Agent : ",o[i].person_id)
                        print("id commands : ",o[i].id_commands)
                        print("commande a exe : ",o[i].data)
                        print("commande status : ", o[i].status)
                        list_ordre_todo[i]= {"Agent":o[i].person_id, "id_commands":o[i].id_commands, "commande a exe ":o[i].data, "commande status":o[i].status}
                        data[o[i].id_commands]={"Agent":o[i].person_id, "id_commands":o[i].id_commands, "commande_status":o[i].status, "commande_exe":o[i].data}
                elif etat == "all":
                    if o[i].data != None:
                        print("Agent : ",o[i].person_id)
                        print("id commands : ",o[i].id_commands)
                        print("commande a exe : ",o[i].data)
                        print("commande status : ", o[i].status)
                        list_ordre_todo[i]= {"Agent":o[i].person_id, "id_commands":o[i].id_commands, "commande a exe ":o[i].data, "commande status":o[i].status}
                        data[o[i].id_commands] = {"Agent": o[i].person_id, "id_commands": o[i].id_commands,
                                                  "commande_status": o[i].status, "commande_exe": o[i].data, "resultat": o[i].resultat}
                elif etat == "done":
                    if o[i].status == True and o[i].data != None:
                        print("Agent : ",o[i].person_id)
                        print("id commands : ",o[i].id_commands)
                        print("commande a exe : ",o[i].data)
                        print("commande status : ", o[i].status)
                        list_ordre_todo[i]= {"Agent":o[i].person_id, "id_commands":o[i].id_commands, "commande a exe ":o[i].data, "commande status":o[i].status}
                        data[o[i].id_commands] = {"Agent": o[i].person_id, "id_commands": o[i].id_commands,
                                                  "commande_status": o[i].status, "commande_exe": o[i].data,"resultat": o[i].resultat}

    return list_ordre_todo,json.dumps(data)
            #return o[i].person_id, o[i].id_commands, o[i].data, o[i].status

#Donne les objets ordre de chaque agent pour tout les agent (c'est de la merde comme fonction)
def listorde(db):
    agentpack = db.session.query(Agent).order_by(Agent.id).all()
    for i in range(len(agentpack)):
        name = agentpack[i].noms
        ordre = agentpack[i].ordre
        print(name + " ", ordre)
        return name , ordre

def sup_ordre(db,id_ordre):
    command = db.session.query(Commands).get(id_ordre)
    if command:
        db.session.delete(command)
        db.session.commit()
        return 'Command supp'
    else:
        return 'Command error 404'

def sup_all_ordre_by_agent(db,id_agent):
    command = db.session.query(Commands).filter_by(person_id=id_agent).all()
    for c in command:
        db.session.delete(command[c])
        db.session.commit()
        return 'Command supp'
    else:
        return 'Command error 404'


def update_status(db,id_ordre,resultats="par defaut"):
    command = db.session.query(Commands).get(id_ordre)
    if command:
        command.status = True
        command.resultat = resultats
        db.session.commit()
        return 'Command status updated'
    else:
        return 'Command error 404'

def view_resultat(db,id_ordre):
    res= db.session.query(Commands).filter_by(id_commands=id_ordre,status=True)
    if res is not None :
        data= {"Agent": res.person_id, "id_commands": res.id_commands,
                    "commande_status": res.status, "commande_exe": res.data,"resultat": res.resultat}
        return json.dumps(data)
    else:
        return 'il y pas encore de resultat'
if __name__ == '__main__':
    #app.run(debug=True, host='10.78.120.68', port=4288)
    createAgent(db)
    #createAgentVide(noms_='baaalo')
    #print("le noms: ",createName(10))
    #print(db.session.query(Agent.noms).all())
    #print(testdb())
    #print(db.session.query().all())