import os
import sys
import requests,time
#url = '10.78.120.68:4288'
url = '192.168.0.30:4288'
# ça donne en format string tout ce que renvoie le endpoint
#requests.text
def view_agent():
    # Donne liste des agents via une requête HTTP GET
    r = requests.get(f'http://{url}/ihm/',params={"exe":"1"})
    agents = r.json()
    for a in agents:
        print(f'[Agent_name]: {agents[a]["Agent_name"]}, [ID_agent]: {agents[a]["ID_agent"]}')
    #print(r.texte)

def view_histo(etat,id_agent):
    # choix du type d'historique de l'agent via une requête HTTP GET
    if etat == 1:
        r = requests.get(f'http://{url}/ihm/', params={"qui": id_agent, "exe": "21"})
        listordre = r.json()
        for a in listordre:
            print(
                f'[Agent_id]: {listordre[a]["Agent"]}, [ID_CMD]: {listordre[a]["id_commands"]}, [Status]: {listordre[a]["commande_status"]}, [CMD]: {listordre[a]["commande_exe"]}\n, [Resultat]: {listordre[a]["resultat"]}')
        print("\n")  # \t fait une tab
    elif etat == 2:
        r = requests.get(f'http://{url}/ihm/', params={"qui": id_agent, "exe": "22"})
        listordre = r.json()
        for a in listordre:
            print(
                f'[Agent_id]: {listordre[a]["Agent"]}, [ID_CMD]: {listordre[a]["id_commands"]}, [Status]: {listordre[a]["commande_status"]}, [CMD]: {listordre[a]["commande_exe"]}')
        print("\n")  # \t fait une tab
    elif etat == 3:
        r = requests.get(f'http://{url}/ihm/', params={"qui": id_agent, "exe": "23"})
        listordre = r.json()
        for a in listordre:
            print(f'[Agent_id]: {listordre[a]["Agent"]}, [ID_CMD]: {listordre[a]["id_commands"]}, [Status]: {listordre[a]["commande_status"]}, [CMD]: {listordre[a]["commande_exe"]}\n, [Resultat]: {listordre[a]["resultat"]}')
        print("\n")  # \t fait une tab
    # parti qui affiche dans l'historique

def send_command(agent_name, command):
    # Envoie la commande à l'agent via une requête HTTP POST
    r=requests.post(f'http://{url}/addCommand/', data={'agent_name': agent_name, 'command': command})
    res =r.json()
    print("\n")
    print(f'[Agent_id]: {res["Agent"]}, [ID_CMD]: {res["id_commands"]},[CMD]: {res["commande_exe"]}')
    print("\n")
    return res["id_commands"]
def get_commands(id_agent):
    # Récupère les commandes de l'agent via une requête HTTP GET
    r = requests.get(f'http://{url}/ihm/',params={"qui": id_agent, "exe": "4"})
    list = r.json()
    for a in list :
        print(f'[Agent_id]: {list[a]["Agent"]}, [ID_CMD]: {list[a]["id_commands"]}, [Status]: {list[a]["commande_status"]}, [CMD]: {list[a]["commande_exe"]}')
    print("\n")

def delete_command(etat,agent_id="",command_id=""):
    # Supprime la commande via une requête HTTP POST
    #requests.delete(f'http://{url}/ihm/{command_id}')
    #url = 'http://192.168.0.30:4288/addCommand/'
    #data = {'agent_name': agent_id, 'command_id': command_id}
    if etat == 1:
        response = requests.post(f'http://{url}/ihm/', data={'agent_name': agent_id, 'command_id': command_id,'exe': '52'})
    elif etat == 2:
        response = requests.post(f'http://{url}/ihm/', data={'agent_name': agent_id, 'command_id': command_id,'exe': '53'})
    elif etat == 3:
        response = requests.post(f'http://{url}/ihm/', data={'agent_name': agent_id, 'command_id': command_id,'exe': '51'})
    else :
        print("problème d etat")
    print(response.text)
    print("\n")

def update_status(command_id,agent_id=""):
    # Met à jour le statut de la commande via une requête HTTP POST
    response = requests.post(f'http://{url}/ihm/', data={'agent_name': agent_id, 'command_id': command_id, "resultat":"Aborde" ,'exe': '6'})

def send_file(agent_name, filepath):
    # Envoie le fichier à l'agent via une requête HTTP POST
    with open(filepath, 'rb') as f:
        requests.post(f'http://{url}/send_file/{agent_name}', files={'file': f})

def get_file(agent_name, filename):
    # Récupère le fichier de l'agent via une requête HTTP GET
    r = requests.get(f'http://{url}/get_file/{agent_name}/{filename}')
    with open(filename, 'wb') as f:
        f.write(r.content)

def main():
    while True:
        # Affiche le menu
        print('1. Lister agents')
        print('2. voir historique')
        print('3. Envoyer une commande')
        print('4. Récupérer la prochaine commandes')
        print('5. Supprimer une commande')
        print('6. Maj commande status Flase to True avec [ID_CMD]')
        print('7. Envoyer un fichier [In coming]')
        print('8. Récupérer un fichier [In coming]')
        print('9. Quitter')

        choice = int(input('Choix: '))

        if choice == 1:
            view_agent()
        elif choice == 2:
            agent_id = input('Nom de l agent: ')
            print('1. voir tout l historique')
            print('2. voir historique des commandes a faire')
            print('3. voir historique des commandes faites')
            print('4. quitter')
            choice = int(input('Choix: '))
            if choice  == 1:
                view_histo(1,agent_id)
            elif choice == 2:
                view_histo(2,agent_id)
            elif choice == 3:
                view_histo(3,agent_id)
            elif choice == 4:
                pass

        elif choice == 3:
            # Envoie une commande
            print('1. envoyer commande a la suite')
            print('2. envoyer commande attendre la reponse [In coming]')
            print('3. quitter')
            choice = int(input('Choix: '))

            if choice  == 1:
                while True:
                    agent_name = input('Nom de l agent: ')
                    command = input('Commande: ')
                    send_command(agent_name, command)
                    # Demande si l'utilisateur souhaite envoyer une autre commande
                    another_command = input('Envoyer une autre commande ? (y/n)')
                    if another_command.lower() != 'y':
                        break
            elif choice == 2:
                agent_name = input('Nom de l agent: ')
                command = input('Commande: ')
                send_command(agent_name, command)

                # Attend jusqu'à ce que le résultat de la commande soit disponible
                while True:
                    response = requests.post(f'http://{url}/ihm/',
                                             data={'agent_name': agent_id, 'command_id': command_id, 'exe': '31'})
                    result = response.json()
                    if 'resultat' in result and result["commande_status"] == True :
                        print(result['resultat'])
                        break
                    else:
                        time.sleep(5)

            elif choice == 3:
                pass



        elif choice == 4:
            # Récupère la prochaine commande
            agent_name = input('Nom de l agent: ')
            commands = get_commands(agent_name)
            """            
            for c in commands:
                print(f'ID: {c["id"]}, Commande: {c["command"]}, Statut: {c["status"]}')
            """
        elif choice == 5:
            # Supprime une commande
            print('1. Supprime une commande avec [ID_CMD]')
            print('2. Supprime la prochaine commande d un agent [In coming]')
            print('3. Supprime toutes les commande d un agent')
            print('4. quitter')
            choice = int(input('Choix: '))
            if choice == 1:
                ID_CMD = int(input('ID_CMD ?: '))
                delete_command(choice, command_id=ID_CMD)
            elif choice == 2:
                id_agent = int(input('ID agent ?: '))
                delete_command(choice, agent_id=id_agent)
            elif choice == 3:
                id_agent = int(input('ID agent ?: '))
                delete_command(choice, agent_id=id_agent)
            elif choice == 4:
                pass
        elif choice == 6:
            # Met à jour le statut d'une commande
            command_id = int(input('ID de la commande: '))
            update_status(command_id,)

        elif choice == 7:
            # Envoie un fichier
            agent_name = input('Nom de l agent: ')
            filepath = input('Chemin du fichier: ')
            send_file(agent_name, filepath)
        elif choice == 8:
            # Récupère un fichier
            agent_name = input('Nom de l agent: ')
            filename = input('Nom du fichier: ')
            get_file(agent_name, filename)
        elif choice == 9:
            # Quitte l'application
            break

if __name__ == '__main__':
    main()
