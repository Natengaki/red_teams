from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/natengaki/Documents/I3/Red_Teams/register/test.db'
db = SQLAlchemy(app)
#db = SQLAlchemy(app)
db.init_app(app)
app.app_context().push()

class Agent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    #ordre = db.Column(db.Integer, db.ForeignKey('commands.orde'),nullable=True)
    ordre = db.relationship('Commands',backref=db.backref('agent', lazy=True))
    noms = db.Column(db.String(256),nullable=True)
    os = db.Column(db.String(256),nullable=True)
    def __repr__(self):
        return '<Agent %r>' % self.id

class Commands(db.Model):
    id_commands = db.Column(db.Integer, primary_key=True)
    orde = db.Column(db.Integer, nullable=True) #Type d'ordre Exécute etc.. format 01, etc..
    data = db.Column(db.String(256),nullable=True)
    status = db.Column(db.Boolean, nullable =True) # cancel = 1, à faire = 0
    resultat = db.Column(db.String(256), nullable=True)
    person_id = db.Column(db.Integer, db.ForeignKey('agent.id'),
                          nullable=False)
    def __repr__(self):
        return '<Commands %r>' % self.orde

if __name__ == '__main__':
    #app.run(debug=True, host='10.78.120.68', port=4288)
    pass